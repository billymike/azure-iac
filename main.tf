terraform {

  required_version = ">=0.12"
  
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = "~>2.0"
    }
  }

    backend "azurerm" {}
}

provider "azurerm" {
  features {}
}

variable "rg-name" {}


module "resource-group" {
    source                      = "git::https://gitlab.com/mikebrumfield-gl/azure-iac-definitions.git"
    resource_group_name_prefix  = var.rg-name
}